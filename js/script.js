function formatatempo(segs) {
  min = 0;
  hr = 0;
  /*
  if hr < 10 then hr = "0"&hr
  if min < 10 then min = "0"&min
  if segs < 10 then segs = "0"&segs
  */
  while(segs>=60) {
    if (segs >=60) {
      segs = segs-60;
      min = min+1;
    }
  }

  while(min>=60) {
    if (min >=60) {
      min = min-60;
      hr = hr+1;
    }
  }

  if (hr < 10) {hr = "0"+hr}
  if (min < 10) {min = "0"+min}
  if (segs < 10) {segs = "0"+segs}
  fin = hr+":"+min+":"+segs
  fin = min+":"+segs
  //fin = segs;
  return fin;
}

var segundos = 20; //inicio do cronometro
var pontoequipea = 0;
var pontoequipeb = 0;
var avisardezsegundos = false;
var isStarted = false;

function conta() {
  segundos--;
  document.getElementById("counter").innerHTML = formatatempo(segundos);
  if (segundos == 10 && avisardezsegundos) {
    playsound('media/beep-curto.mp3');
  }
  if (segundos == 0) {
    playsound('media/Buzzer.mp3');
    parar();
  }
}

function iniciar(){
  if (segundos > 0) {
    interval = setInterval("conta();",1000);
    isStarted = true;
    habilitarbtniniciar(true);
  }
}

function parar(){
  if (isStarted)
  clearInterval(interval);
  habilitarbtniniciar(false);
}

function habilitarbtniniciar(habilita) {
  document.getElementById("btniniciar").disabled = habilita;
}

function zerar(){
  if (isStarted)
  clearInterval(interval);

  segundos = 20;
  document.getElementById("counter").innerHTML = formatatempo(segundos);
  habilitarbtniniciar(false);
}

function atribuipontoequipea(ponto){
  if ((ponto > 0 && pontoequipea >= 0) || (ponto < 0 && pontoequipea > 0)) {
    pontoequipea = pontoequipea + ponto;
    document.getElementById("pontosequipea").innerHTML = pontoequipea;
    zerar();
  }
}

function atribuipontoequipeb(ponto){
  if ((ponto > 0 && pontoequipeb >= 0) || (ponto < 0 && pontoequipeb > 0)) {
    pontoequipeb = pontoequipeb + ponto;
    document.getElementById("pontosequipeb").innerHTML = pontoequipeb;
    zerar();
  }
}

function virarquadra(){
  var pontotemp = 0;
  var equipetemp;

  pontotemp = document.getElementById("pontosequipeb").innerHTML;
  document.getElementById("pontosequipeb").innerHTML = document.getElementById("pontosequipea").innerHTML;
  document.getElementById("pontosequipea").innerHTML = pontotemp;
  pontoequipeb = parseInt(pontoequipea);
  pontoequipea = parseInt(pontotemp);

  equipetemp = document.getElementsByName('nomeequipeb')[0].value;
  document.getElementsByName('nomeequipeb')[0].value = document.getElementsByName('nomeequipea')[0].value;
  document.getElementsByName('nomeequipea')[0].value = equipetemp;
}

function setavisardezsegundos() {
  avisardezsegundos = document.getElementsByName('chkavisadezsegundos')[0].checked;
}

function resetar() {
  zerar();
  pontoequipea = 0;
  document.getElementById("pontosequipea").innerHTML = pontoequipea;
  pontoequipeb = 0;
  document.getElementById("pontosequipeb").innerHTML = pontoequipeb;
  document.getElementsByName("nomeequipea")[0].value = "HOME";
  document.getElementsByName('nomeequipeb')[0].value = "VISITORS";
}

function playsound(audio){
  var audio = new Audio(audio);
  audio.play();
}
